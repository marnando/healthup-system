#-------------------------------------------------
#
# Project created by QtCreator 2015-06-22T21:33:53
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = HealthUp
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++0x

addFiles.sources = qrc/database/healthup_database.db3

SOURCES +=  src/main.cpp \
    src/front/controller.cpp \
    src/dao/connection.cpp \
    src/view/mainwindow.cpp \
    src/view/controlealunos.cpp \
    src/view/controllerwidget.cpp \
    src/control/ccontrolealunos.cpp \
    src/model/ncontrolealunos.cpp \
    src/utils/uiutils.cpp \
    src/view/promote/mtablewidget.cpp

HEADERS  += src/dao/connection.h \
    src/front/controller.h \
    src/utils/defines.h \
    src/view/mainwindow.h \
    src/view/controlealunos.h \
    src/view/controllerwidget.h \
    src/control/ccontrolealunos.h \
    src/model/ncontrolealunos.h \
    src/utils/uiutils.h \
    src/view/promote/mtablewidget.h

FORMS    += qrc/ui/mainwindow.ui \
    qrc/ui/alunocontrole.ui \
    qrc/ui/controle.ui \
    qrc/ui/mtablewidget.ui

RESOURCES += \
    qrc/uis.qrc \
    qrc/icons.qrc \
    qrc/qss.qrc
