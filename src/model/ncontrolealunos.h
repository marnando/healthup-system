#ifndef NCONTROLEALUNOS_H
#define NCONTROLEALUNOS_H

#include <QObject>
#include <QMap>

class NControleAlunos : public QObject
{
    Q_OBJECT

private:
    QMap<quint32, QString> m_mapAluno;

public:
    explicit NControleAlunos(QObject *parent = 0);
    virtual ~NControleAlunos();

    bool insertAluno(QMap<quint32, QString> a_mapAluno);
    QList<QList<QVariant> > createListAlunos();

signals:

public slots:
};

#endif // NCONTROLEALUNOS_H
