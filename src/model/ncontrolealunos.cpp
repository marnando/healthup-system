#include "src/dao/connection.h"
#include "src/utils/defines.h"
#include "ncontrolealunos.h"
#include <QSqlQuery>
#include <QList>

NControleAlunos::NControleAlunos(QObject *parent) : QObject(parent)
{

}

NControleAlunos::~NControleAlunos()
{

}

bool NControleAlunos::insertAluno(QMap<quint32, QString> a_mapAluno)
{
    QString table = QString("t_alunos");
    QList<QVariant> listOfData;
    QList<QString>  listOfBind;

    listOfData << a_mapAluno[CTRL_ALUNO_NOME]
               << a_mapAluno[CTRL_ALUNO_ENDERECO]
               << a_mapAluno[CTRL_ALUNO_BAIRRO]
               << a_mapAluno[CTRL_ALUNO_CIDADE]
               << a_mapAluno[CTRL_ALUNO_CEP]
               << a_mapAluno[CTRL_ALUNO_UF]
               << a_mapAluno[CTRL_ALUNO_TELEFONE]
               << a_mapAluno[CTRL_ALUNO_CELULAR]
               << a_mapAluno[CTRL_ALUNO_SEXO]
               << a_mapAluno[CTRL_ALUNO_CPF]
               << a_mapAluno[CTRL_ALUNO_RG]
               << a_mapAluno[CTRL_ALUNO_ORGAO]
               << a_mapAluno[CTRL_ALUNO_EMAIL]
               << a_mapAluno[CTRL_ALUNO_NASCIMENTO]
               << a_mapAluno[CTRL_ALUNO_IDADE]
               << 0;

    listOfBind << ":s_nome"
               << ":s_endereco"
               << ":s_bairro"
               << ":s_cidade"
               << ":u_cep"
               << ":s_uf"
               << ":u_fone"
               << ":u_celular"
               << ":s_sexo"
               << ":u_cpf"
               << ":u_rg"
               << ":s_orgao_exp"
               << ":s_mail"
               << ":u_nascimento"
               << ":u_idade"
               << ":u_status";

    return Connection::m_connection->insert(table, listOfData, listOfBind);
}

QList<QList<QVariant> > NControleAlunos::createListAlunos()
{
    QString table = QString("t_alunos");
    QList<QVariant> listOfData;
    QList<QString>  listOfBind;
    QList<QList<QVariant> > list = Connection::m_connection->selectAll(table, listOfData, listOfBind);
    return list;
}

