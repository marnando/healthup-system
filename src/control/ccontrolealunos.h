#ifndef CCONTROLEALUNOS_H
#define CCONTROLEALUNOS_H

#include <QObject>
#include <QMap>
#include <QVariant>
#include <QList>

class NControleAlunos;
class CControleAlunos : public QObject
{
    Q_OBJECT

private:
    NControleAlunos * m_nAlunos;

public:
    CControleAlunos(QObject *parent = 0);
    virtual ~CControleAlunos();

    NControleAlunos * nControleAlunos();

signals:
    void listDataControleAlunos(QList<QList<QVariant> >);
    void listDataAlunosDebito(QList<QList<QVariant> >);
    void listDataAlunosAusentes(QList<QList<QVariant> >);

public slots:
    void onSalvarCadastro(QMap<quint32, QString> a_mapAluno);
    void onEditarCadastro();
    void onDeleteCadastro();

    void onCriarListaControleAlunos();
    void onCriarListaAlunosDebito();
    void onCriarListaAlunosAusentes();
};

#endif // CCONTROLEALUNOS_H
