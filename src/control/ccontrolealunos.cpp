#include "ccontrolealunos.h"
#include "src/model/ncontrolealunos.h"

CControleAlunos::CControleAlunos(QObject * a_parent)
    : QObject(a_parent),
      m_nAlunos(nullptr)
{

}

CControleAlunos::~CControleAlunos()
{

}

NControleAlunos *CControleAlunos::nControleAlunos()
{
    return m_nAlunos;
}

void CControleAlunos::onSalvarCadastro(QMap<quint32, QString> a_mapAluno)
{
    m_nAlunos->insertAluno(a_mapAluno);
}

void CControleAlunos::onEditarCadastro()
{

}

void CControleAlunos::onDeleteCadastro()
{

}

void CControleAlunos::onCriarListaControleAlunos()
{
    QList<QList<QVariant> > listDataModel;
    listDataModel = m_nAlunos->createListAlunos();
    emit listDataControleAlunos(listDataModel);
}

void CControleAlunos::onCriarListaAlunosDebito()
{
    QList<QList<QVariant> > listDataModel;
    listDataModel = m_nAlunos->createListAlunos();
    emit listDataAlunosDebito(listDataModel);
}

void CControleAlunos::onCriarListaAlunosAusentes()
{
    QList<QList<QVariant> > listDataModel;
    listDataModel = m_nAlunos->createListAlunos();
    emit listDataAlunosAusentes(listDataModel);
}

