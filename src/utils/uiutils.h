#ifndef UIUTILS_H
#define UIUTILS_H

#include <QObject>

class UiUtils : public QObject
{
    Q_OBJECT
private:
    static UiUtils * m_utils;

public:
    explicit UiUtils(QObject * parent = 0);
    virtual ~UiUtils();

    static UiUtils * instance();
    void release();
    void contraliseMeuWidget(QWidget & a_wid);
    void resizeWidget(QWidget & a_wid, float a_heightFactor = 0.70f, float a_widthFactor = 0.60f);

signals:

public slots:
};

#endif // UIUTILS_H
