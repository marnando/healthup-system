#ifndef DEFINES
#define DEFINES


/*
 * Defines módulo
 */

#define DEF_SHOW_BTN_LIMPAR         0x00000001UL
#define DEF_SHOW_BTN_NOVO_CADASTRO  0x00000002UL
#define DEF_SHOW_BTN_EXCLUIR        0x00000004UL
#define DEF_SHOW_BTN_EDITAR         0x00000008UL
#define DEF_SHOW_BTN_SALVAR         0x00000010UL

/*
 * Defines módulo Aluno
 */

#define CTRL_ALUNO_MATRICULA        0x00000001UL
#define CTRL_ALUNO_NOME             (CTRL_ALUNO_MATRICULA + 1)
#define CTRL_ALUNO_ENDERECO         (CTRL_ALUNO_NOME + 1)
#define CTRL_ALUNO_BAIRRO           (CTRL_ALUNO_ENDERECO + 1)
#define CTRL_ALUNO_CEP              (CTRL_ALUNO_BAIRRO + 1)
#define CTRL_ALUNO_CIDADE           (CTRL_ALUNO_CEP + 1)
#define CTRL_ALUNO_UF               (CTRL_ALUNO_CIDADE + 1)
#define CTRL_ALUNO_TELEFONE         (CTRL_ALUNO_UF + 1)
#define CTRL_ALUNO_CELULAR          (CTRL_ALUNO_TELEFONE + 1)
#define CTRL_ALUNO_SEXO             (CTRL_ALUNO_CELULAR + 1)
#define CTRL_ALUNO_CPF              (CTRL_ALUNO_SEXO + 1)
#define CTRL_ALUNO_RG               (CTRL_ALUNO_CPF + 1)
#define CTRL_ALUNO_ORGAO            (CTRL_ALUNO_RG + 1)
#define CTRL_ALUNO_EMAIL            (CTRL_ALUNO_ORGAO + 1)
#define CTRL_ALUNO_NASCIMENTO       (CTRL_ALUNO_EMAIL + 1)
#define CTRL_ALUNO_IDADE            (CTRL_ALUNO_NASCIMENTO + 1)
#define CTRL_ALUNO_STATUS           (CTRL_ALUNO_IDADE + 1)

// QTabWidget indexes
#define LIST_CONTROLE_ALUNO         0U
#define LIST_ALUNOS_DEBITO          1U
#define LIST_ALUNOS_AUSENTES        2U




#endif // DEFINES
