#include "uiutils.h"
#include <QApplication>
#include <QDesktopWidget>
#include <QWidget>

UiUtils * UiUtils::m_utils = nullptr;
UiUtils::UiUtils(QObject *parent) : QObject(parent)
{

}

UiUtils::~UiUtils()
{
    release();
}

UiUtils * UiUtils::instance()
{
    if (! m_utils)
    {
        m_utils = new UiUtils();
    }

    return m_utils;
}

void UiUtils::release()
{
    if (m_utils)
    {
        delete m_utils;
        m_utils = nullptr;
    }
}

void UiUtils::contraliseMeuWidget(QWidget & a_wid)
{
    Q_UNUSED(a_wid)
}

void UiUtils::resizeWidget(QWidget & a_wid, float a_heightFactor, float a_widthFactor)
{
    a_wid.setWindowFlags(Qt::Dialog);
    a_wid.setWindowModality(Qt::WindowModal);
    a_wid.setMinimumHeight(QApplication::desktop()->height() * a_heightFactor);
    a_wid.setMinimumWidth(QApplication::desktop()->width() * a_widthFactor);
    a_wid.show();
}

