#include "ui_controle.h"
#include "src/utils/defines.h"
#include "src/utils/uiutils.h"
#include "controllerwidget.h"

ControllerWidget::ControllerWidget(QWidget *parent)
                : QWidget(parent)
                , m_widChild(nullptr)
                , ui(new Ui::WidController)
{
    setWidAttribute();
}

ControllerWidget::ControllerWidget(QWidget *parent, QWidget *a_child)
                : QWidget(parent)
                , m_widChild(nullptr)
                , ui(new Ui::WidController)
{
    setWidAttribute();
    insertWidget(a_child);
}

ControllerWidget::~ControllerWidget()
{
    delete ui;
}

void ControllerWidget::insertWidget(QWidget *a_widget)
{
    if (a_widget)
    {
        m_widChild = a_widget;
        m_widChild->setParent(this);
        ui->m_controllerLayout->addWidget(m_widChild);
    }
}

void ControllerWidget::setWindowInformation(QIcon a_icon, QString a_title)
{
    setWindowIcon(a_icon);
    setWindowTitle(a_title);
}

void ControllerWidget::closeEvent(QCloseEvent *a_event)
{
    emit closeControleAlunos();
    QWidget::closeEvent(a_event);
}

void ControllerWidget::setWidAttribute()
{
    ui->setupUi(this);
    UiUtils::instance()->resizeWidget(*this, 0.6f);
    emit atualizarBotoesMenuInferior();
}

void ControllerWidget::onExibirBotoesDeMenu(qulonglong a_flag)
{
    ui->m_btnLimpar->setVisible((a_flag & DEF_SHOW_BTN_LIMPAR) == DEF_SHOW_BTN_LIMPAR);
    ui->m_btnNovoCadastro->setVisible((a_flag & DEF_SHOW_BTN_NOVO_CADASTRO) == DEF_SHOW_BTN_NOVO_CADASTRO);
    ui->m_btnExcluir->setVisible((a_flag & DEF_SHOW_BTN_EXCLUIR) == DEF_SHOW_BTN_EXCLUIR);
    ui->m_btnEditar->setVisible((a_flag & DEF_SHOW_BTN_EDITAR) == DEF_SHOW_BTN_EDITAR);
    ui->m_btnSalvar->setVisible((a_flag & DEF_SHOW_BTN_SALVAR) == DEF_SHOW_BTN_SALVAR);
}

void ControllerWidget::on_m_btnNovoCadastro_clicked()
{
    emit novoCadastroClicked();
}

void ControllerWidget::on_m_btnLimpar_clicked()
{
    emit limparDadosClicked();
}

void ControllerWidget::on_m_btnExcluir_clicked()
{
    emit excluirDadosClicked();
}

void ControllerWidget::on_m_btnEditar_clicked()
{
    emit editarDadosClicked();
}

void ControllerWidget::on_m_btnSalvar_clicked()
{
    emit salvarDadosClicked();
}
