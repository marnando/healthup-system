#include "ui_alunocontrole.h"
#include "src/utils/defines.h"
#include "controlealunos.h"
#include <QDebug>
#include <QMessageBox>

ControleAlunos::ControleAlunos(QWidget * a_parent, int a_indexTabWidget) :
    QWidget(a_parent),
    ui(new Ui::AlunoControle)
{
    ui->setupUi(this);
    m_indexTab = 0;
    init(a_indexTabWidget);
}

ControleAlunos::~ControleAlunos()
{
    delete ui;
}

void ControleAlunos::createDataList()
{
    switch (m_indexTab) {

    case LIST_CONTROLE_ALUNO:
    {
        emit criarListaAlunos();
    }
        break;
    case LIST_ALUNOS_DEBITO:
    {
        emit criarListaAlunosDebito();
    }
        break;
    case LIST_ALUNOS_AUSENTES:
    {
        emit criarListaAlunosAusentes();
    }
        break;
    default:
        break;
    }
}

void ControleAlunos::setComboBoxIndex()
{
    ui->m_cbxUf->addItems(QStringList() << "AC" << "AL" << "AP" << "AM" << "BA" << "CE" << "DF" << "ES" << "GO"
                                        << "MA" << "MT" << "MS" << "MG" << "PA" << "PB" << "PR" << "PE" << "PI"
                                        << "RJ" << "RN" << "RS" << "RO" << "RR" << "SC" << "SP" << "SE" << "TO");

    ui->m_cbxSexo->addItems(QStringList() << "Masculino" << "Feminino");
}

void ControleAlunos::setTableWidgetAttributes(int a_indexTabWidget, std::initializer_list<QTableWidget *> a_list)
{
    ui->m_tableWidAlunos->setCurrentIndex(a_indexTabWidget);

    foreach (QTableWidget * table, a_list) {
        table->verticalHeader()->setVisible(false);
        table->setEditTriggers(QAbstractItemView::NoEditTriggers);
        table->setSelectionBehavior(QAbstractItemView::SelectRows);
        table->setSelectionMode(QAbstractItemView::SingleSelection);
        table->setStyleSheet("QTableView {selection-background-color: steelblue;}");
    }
}

void ControleAlunos::init(int a_indexTabWidget)
{
    m_indexTab = a_indexTabWidget;

    setTableWidgetAttributes(a_indexTabWidget, {ui->m_tablewAlunosControle, ui->m_tablewAlunosAusentes, ui->m_tablewAlunosDebito});
    setComboBoxIndex();

    connect(ui->m_tableWidAlunos, SIGNAL(currentChanged(int)), this, SLOT(onMudarBotoesComAcaoDaTab(int)));
    connect(ui->m_tableWidAlunos, SIGNAL(currentChanged(int)), this, SLOT(onRefreshModel(int)));
}

bool ControleAlunos::validateFields()
{
    if (ui->m_leNome->text().isEmpty()
    ||  ui->m_leEndereco->text().isEmpty()
    ||  ui->m_leBairro->text().isEmpty()
    ||  ui->m_leCidade->text().isEmpty()
    ||  ui->m_leTelefone->text().isEmpty()
    ||  ui->m_leCelular->text().isEmpty()
    ||  ui->m_dateNascimento->text().isEmpty()
    ||  ui->m_cbxUf->currentText().isEmpty()
    ||  ui->m_cbxSexo->currentText().isEmpty())
    {
        QMessageBox::information(this,
                                 tr("Atenção"),
                                 tr("Necessário preenchimento de todos os dados marcados com (\" * \") para o cadastro."),
                                 QMessageBox::Yes);
        return false;
    }

    return true;
}

bool ControleAlunos::prepararMapDados()
{
    bool ok = false;

    m_mapAluno.insert(CTRL_ALUNO_MATRICULA, ui->m_leMatricula->text());
    m_mapAluno.insert(CTRL_ALUNO_NOME, ui->m_leNome->text());
    m_mapAluno.insert(CTRL_ALUNO_ENDERECO, ui->m_leEndereco->text());
    m_mapAluno.insert(CTRL_ALUNO_BAIRRO, ui->m_leBairro->text());
    m_mapAluno.insert(CTRL_ALUNO_CEP, ui->m_leCep->text());
    m_mapAluno.insert(CTRL_ALUNO_CIDADE, ui->m_leCidade->text());
    m_mapAluno.insert(CTRL_ALUNO_UF, ui->m_cbxUf->currentText());
    m_mapAluno.insert(CTRL_ALUNO_TELEFONE, ui->m_leTelefone->text());
    m_mapAluno.insert(CTRL_ALUNO_CELULAR, ui->m_leCelular->text());
    m_mapAluno.insert(CTRL_ALUNO_SEXO, ui->m_cbxSexo->currentText());
    m_mapAluno.insert(CTRL_ALUNO_CPF, ui->m_leCpf->text());
    m_mapAluno.insert(CTRL_ALUNO_RG, ui->m_leRg->text());
    m_mapAluno.insert(CTRL_ALUNO_ORGAO, ui->m_leOrgao->text());
    m_mapAluno.insert(CTRL_ALUNO_EMAIL, ui->m_leEmail->text());
    m_mapAluno.insert(CTRL_ALUNO_NASCIMENTO, ui->m_dateNascimento->text());
    m_mapAluno.insert(CTRL_ALUNO_IDADE, ui->m_leIdade->text());
    m_mapAluno.insert(CTRL_ALUNO_SEXO, ui->m_leCelular->text());

    emit salvarCadastroAluno(m_mapAluno);

    return ok;
}

void ControleAlunos::closeEvent(QCloseEvent * a_event)
{
    QWidget::closeEvent(a_event);
}

void ControleAlunos::onCriarListaControleAlunos(QList<QList<QVariant> > a_listDataModel)
{
    if (a_listDataModel.size() > 0)
    {
        int row = 0;
        int col = 0;

        ui->m_tablewAlunosControle->setRowCount(a_listDataModel.size());

        foreach (QList<QVariant> list, a_listDataModel) {
            foreach (QVariant var, list) {
                ui->m_tablewAlunosControle->setItem(row, col, new QTableWidgetItem(var.toString()));
                col++;
            }
            row++;
            col = 0;
        }
    }
}

void ControleAlunos::onCriarListaAlunosDebito(QList<QList<QVariant> > a_listDataModel)
{
    if (a_listDataModel.size() > 0)
    {
        int row = 0;
        int col = 0;

        ui->m_tablewAlunosDebito->setRowCount(a_listDataModel.size());

        foreach (QList<QVariant> list, a_listDataModel) {
            foreach (QVariant var, list) {
                ui->m_tablewAlunosDebito->setItem(row, col, new QTableWidgetItem(var.toString()));
                col++;
            }
            row++;
            col = 0;
        }
    }
}

void ControleAlunos::onCriarListaAlunosAusentes(QList<QList<QVariant> > a_listDataModel)
{
    if (a_listDataModel.size() > 0)
    {
        int row = 0;
        int col = 0;

        ui->m_tablewAlunosAusentes->setRowCount(a_listDataModel.size());

        foreach (QList<QVariant> list, a_listDataModel) {
            foreach (QVariant var, list) {
                ui->m_tablewAlunosAusentes->setItem(row, col, new QTableWidgetItem(var.toString()));
                col++;
            }
            row++;
            col = 0;
        }
    }
}

void ControleAlunos::onSalvarCadastroAluno()
{
    if (validateFields())
    {
        prepararMapDados();
    }
}

void ControleAlunos::on_m_btnCalendar_clicked()
{

}

void ControleAlunos::onMudarBotoesComAcaoDaTab(int a_tabIndex)
{
    qulonglong flag = 0;

    switch (a_tabIndex) {
    case 0:
    case 1:
    case 2:
    {
        flag = DEF_SHOW_BTN_EDITAR |
                DEF_SHOW_BTN_EXCLUIR |
                DEF_SHOW_BTN_NOVO_CADASTRO;
    }
        break;
    case 3:
    {
        flag = DEF_SHOW_BTN_LIMPAR |
                DEF_SHOW_BTN_SALVAR |
                DEF_SHOW_BTN_EXCLUIR;
    }
        break;
    default:
        break;
    }

    emit exibirBotoesDeMenu(flag);
}

void ControleAlunos::onAtualizarBotoesMenuInferior()
{
    onMudarBotoesComAcaoDaTab(0);
}

void ControleAlunos::onRefreshModel(int a_tabIndex)
{
    m_indexTab = a_tabIndex;
    createDataList();
}
