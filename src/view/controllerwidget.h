#ifndef CONTROLLERWIDGET_H
#define CONTROLLERWIDGET_H

#include <QWidget>
#include <QCloseEvent>

namespace Ui {
    class WidController;
}

class ControllerWidget : public QWidget
{
    Q_OBJECT

private:
    QWidget * m_widChild;

    Ui::WidController * ui;

public:
    explicit ControllerWidget(QWidget *parent = 0);
    explicit ControllerWidget(QWidget *parent = 0, QWidget * a_child = 0);
    ~ControllerWidget();

    void insertWidget(QWidget * a_widget);
    void setWindowInformation(QIcon a_icon, QString a_title);

    void closeEvent(QCloseEvent * a_event);

protected:
    void setWidAttribute();

signals:
    void closeControleAlunos();
    void atualizarBotoesMenuInferior();
    void novoCadastroClicked();
    void limparDadosClicked();
    void excluirDadosClicked();
    void editarDadosClicked();
    void salvarDadosClicked();

public slots:
    void onExibirBotoesDeMenu(qulonglong a_flag);
    void on_m_btnNovoCadastro_clicked();
    void on_m_btnLimpar_clicked();
    void on_m_btnExcluir_clicked();
    void on_m_btnEditar_clicked();
    void on_m_btnSalvar_clicked();
};

#endif // CONTROLLERWIDGET_H
