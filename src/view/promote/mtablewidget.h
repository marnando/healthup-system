#ifndef MTABLEWIDGET_H
#define MTABLEWIDGET_H

#include <QObject>
#include <QTableWidget>

class MTableWidget : public QTableWidget
{
    Q_OBJECT

public:
    explicit MTableWidget(QTableWidget * parent = 0);
    virtual ~MTableWidget();

signals:
public slots:
};

#endif // MTABLEWIDGET_H
