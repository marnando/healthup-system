#include "ui_mainwindow.h"
#include "src/utils/defines.h"
#include "mainwindow.h"
#include "qmdisubwindow.h"
#include <QMessageBox>
#include <QDebug>

MainWindow::MainWindow(QWidget * a_parent) :
    QMainWindow( a_parent),
    m_widController(nullptr),
    m_alunos(nullptr),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setConnections();
}

MainWindow::~MainWindow()
{
    if (m_widController) { delete m_alunos; m_alunos = nullptr; }
    if (m_alunos) { delete m_alunos; m_alunos = nullptr; }
    delete ui;
}

void MainWindow::setConnections()
{
    alunosConnections();
    treinosConnections();
    financeiroConnections();
    ferramentasConnections();
    relatoriosConnections();
    sobreConnections();
    sairConnection();
    toolBarConnections();
}

void MainWindow::setControlWidget(QWidget * a_child, QIcon a_icon, QString a_title)
{
    m_widController = new ControllerWidget(this, a_child);
    m_widController->setWindowInformation(a_icon, a_title);
    connect(m_widController, SIGNAL(closeControleAlunos()), SLOT(onCloseControleAlunos()));
}

ControleAlunos * MainWindow::controleAlunos()
{
    return m_alunos;
}

void MainWindow::alunosConnections()
{
    //Abertura das sub-módulos
    connect(ui->actionAlunos,           SIGNAL(triggered(bool)), this, SLOT(onOpenAlunos()));
    connect(ui->actionAlunosDebito,     SIGNAL(triggered(bool)), this, SLOT(onOpenAlunosDebito()));
    connect(ui->actionAlunosAusentes,   SIGNAL(triggered(bool)), this, SLOT(onOpenAlunosAusentes()));
    connect(ui->actionMatriculas,       SIGNAL(triggered(bool)), this, SLOT(onModuloEmDesenvolvimento()));
    connect(ui->actionAniversariantes,  SIGNAL(triggered(bool)), this, SLOT(onModuloEmDesenvolvimento()));
}

void MainWindow::treinosConnections()
{
    connect(ui->actionModalidades,      SIGNAL(triggered(bool)), this, SLOT(onModuloEmDesenvolvimento()));
    connect(ui->actionAvalicaoFisica,   SIGNAL(triggered(bool)), this, SLOT(onModuloEmDesenvolvimento()));
}

void MainWindow::financeiroConnections()
{
    connect(ui->actionCaixa,            SIGNAL(triggered(bool)), this, SLOT(onModuloEmDesenvolvimento()));
    connect(ui->actionContas_a_Pagar,   SIGNAL(triggered(bool)), this, SLOT(onModuloEmDesenvolvimento()));
    connect(ui->actionContas_a_Receber, SIGNAL(triggered(bool)), this, SLOT(onModuloEmDesenvolvimento()));
}

void MainWindow::ferramentasConnections()
{
    connect(ui->actionFuncionarios,         SIGNAL(triggered(bool)), this, SLOT(onModuloEmDesenvolvimento()));
    connect(ui->actionAuditoria,            SIGNAL(triggered(bool)), this, SLOT(onModuloEmDesenvolvimento()));
    connect(ui->actionAgendaCompromissos,   SIGNAL(triggered(bool)), this, SLOT(onModuloEmDesenvolvimento()));
    connect(ui->actionAgendaTelefonica,     SIGNAL(triggered(bool)), this, SLOT(onModuloEmDesenvolvimento()));
    connect(ui->actionSistema,              SIGNAL(triggered(bool)), this, SLOT(onModuloEmDesenvolvimento()));
    connect(ui->actionBackup,               SIGNAL(triggered(bool)), this, SLOT(onModuloEmDesenvolvimento()));
}

void MainWindow::relatoriosConnections()
{
    connect(ui->actionGerarRelatorios, SIGNAL(triggered(bool)), this, SLOT(onModuloEmDesenvolvimento()));
}

void MainWindow::sobreConnections()
{
    connect(ui->actionAjuda,                SIGNAL(triggered(bool)), this, SLOT(onModuloEmDesenvolvimento()));
    connect(ui->actionHealthUp_System_ADM,  SIGNAL(triggered(bool)), this, SLOT(onModuloEmDesenvolvimento()));
}

void MainWindow::sairConnection()
{
    connect(ui->actionSair, SIGNAL(triggered(bool)), this, SLOT(close()));
}

void MainWindow::toolBarConnections()
{

}

void MainWindow::onModuloEmDesenvolvimento()
{
    QMessageBox::information(this,
                             tr("Atenção"),
                             tr("Esse módulo encontra-se em desenvolvimento."),
                             QMessageBox::Ok);
}

void MainWindow::createAlunos(int a_tabIndex)
{
    if (m_alunos == nullptr)
    {
        m_alunos = new ControleAlunos(m_widController, a_tabIndex);
        setControlWidget(m_alunos, QIcon(QPixmap(":/24/images/24x24/user_group.png")), tr("Controle de Alunos"));
        emit createAlunoControler();

        //Exibição dos menus inferiores
        qDebug() << "MainWindow::onOpenAlunos connect 1: " <<
        connect(m_widController, SIGNAL(atualizarBotoesMenuInferior()), m_alunos, SLOT(onAtualizarBotoesMenuInferior()));
        connect(m_alunos, SIGNAL(exibirBotoesDeMenu(qulonglong)), m_widController, SLOT(onExibirBotoesDeMenu(qulonglong)));

        //Eventos dos botões
        connect(m_widController, SIGNAL(salvarDadosClicked()), m_alunos, SLOT(onSalvarCadastroAluno()) );

        m_alunos->onMudarBotoesComAcaoDaTab(a_tabIndex);
        m_alunos->createDataList();

    }
}

void MainWindow::onOpenAlunos()
{
    createAlunos(LIST_CONTROLE_ALUNO);
}

void MainWindow::onOpenAlunosDebito()
{
    createAlunos(LIST_ALUNOS_DEBITO);
}

void MainWindow::onOpenAlunosAusentes()
{
    createAlunos(LIST_ALUNOS_AUSENTES);
}

void MainWindow::onOpenMatriculas()
{

}

void MainWindow::onOpenAniversariantes()
{

}

void MainWindow::onOpenModalidades()
{

}

void MainWindow::onOpenTreinos()
{

}

void MainWindow::onOpenCaixa()
{

}

void MainWindow::onOpenCaixaAPagar()
{

}

void MainWindow::onOpenCaixaAReceber()
{

}

void MainWindow::onOpenGestaoFinanceira()
{

}

void MainWindow::onOpenFuncionarios()
{

}

void MainWindow::onOpenAuditoria()
{

}

void MainWindow::onOpenAgendaCompromissos()
{

}

void MainWindow::onOpenAgendaTelefonica()
{

}

void MainWindow::onOpenSistema()
{

}

void MainWindow::onOpenBackup()
{

}

void MainWindow::onOpenGerarRelatorios()
{

}

void MainWindow::onOpenAjuda()
{

}

void MainWindow::onOpenSobreHealthUp()
{

}

void MainWindow::onCloseControleAlunos()
{
    delete m_alunos;
    m_alunos = nullptr;
}

void MainWindow::onCloseTreinos()
{

}

void MainWindow::onCloseFinanceiro()
{

}

void MainWindow::onCloseFerramentas()
{

}

void MainWindow::onCloseRelatorios()
{

}

void MainWindow::onCloseSobre()
{

}
