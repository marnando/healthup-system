#ifndef CONTROLEALUNOS_H
#define CONTROLEALUNOS_H

#include <initializer_list>
#include <QWidget>
#include <QMap>
#include <QCloseEvent>
#include <QTableWidget>

#define DEF_SHOW_BTN_LIMPAR         0x00000001UL
#define DEF_SHOW_BTN_NOVO_CADASTRO  0x00000002UL
#define DEF_SHOW_BTN_EXCLUIR        0x00000004UL
#define DEF_SHOW_BTN_EDITAR         0x00000008UL
#define DEF_SHOW_BTN_SALVAR         0x00000010UL

namespace Ui {
    class AlunoControle;
}

class ControleAlunos : public QWidget
{
    Q_OBJECT

public:
    QMap<quint32, QString> m_mapAluno;

private:
    Ui::AlunoControle * ui;
    int m_indexTab;

public:
    explicit ControleAlunos(QWidget * a_parent = 0, int a_indexTabWidget = 0);
    ~ControleAlunos();

    void createDataList();
    void setComboBoxIndex();
    void setTableWidgetAttributes(int a_indexTabWidget, std::initializer_list<QTableWidget *> a_list);

private:
    void init(int a_indexTabWidget);
    bool validateFields();
    bool prepararMapDados();

    void closeEvent(QCloseEvent * a_event);

signals:
    void showBotoesMenu();
    void salvarCadastroAluno(QMap<quint32, QString>);
    void exibirBotoesDeMenu(qulonglong);

    //Criar os controles
    void criarControleAlunos();

    //Criar as listas
    void criarListaAlunos();
    void criarListaAlunosDebito();
    void criarListaAlunosAusentes();

public slots:
    void onCriarListaControleAlunos(QList<QList<QVariant> > a_listDataModel);
    void onCriarListaAlunosDebito(QList<QList<QVariant> > a_listDataModel);
    void onCriarListaAlunosAusentes(QList<QList<QVariant> > a_listDataModel);
    void onSalvarCadastroAluno();
    void onMudarBotoesComAcaoDaTab(int a_tabIndex);
    void onAtualizarBotoesMenuInferior();
    void onRefreshModel(int a_tabIndex);
    void on_m_btnCalendar_clicked();
};

#endif // CONTROLEALUNOS_H
