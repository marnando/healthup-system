#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "controlealunos.h"
#include "controllerwidget.h"
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    ControllerWidget * m_widController;
    ControleAlunos * m_alunos;

    Ui::MainWindow * ui;

public:
    explicit MainWindow(QWidget * a_parent = 0);
    ~MainWindow();

    void setConnections();
    void setControlWidget(QWidget * a_child, QIcon a_icon, QString a_title);
    ControleAlunos * controleAlunos();

    void createAlunos(int a_tabIndex);
protected:
    void alunosConnections();
    void treinosConnections();
    void financeiroConnections();
    void ferramentasConnections();
    void relatoriosConnections();
    void sobreConnections();
    void sairConnection();
    void toolBarConnections();

signals:
    void createAlunoControler();

private slots:

    void onModuloEmDesenvolvimento();

    //Alunos
    void onOpenAlunos();
    void onOpenAlunosDebito();
    void onOpenAlunosAusentes();
    void onOpenMatriculas();
    void onOpenAniversariantes();

    //Treinos
    void onOpenModalidades();
    void onOpenTreinos();

    //Financeiro
    void onOpenCaixa();
    void onOpenCaixaAPagar();
    void onOpenCaixaAReceber();
    void onOpenGestaoFinanceira();

    //Ferramentas
    void onOpenFuncionarios();
    void onOpenAuditoria();
    void onOpenAgendaCompromissos();
    void onOpenAgendaTelefonica();
    void onOpenSistema();
    void onOpenBackup();

    //Relatorios
    void onOpenGerarRelatorios();

    //Sobre
    void onOpenAjuda();
    void onOpenSobreHealthUp();

    //Controle de close window para o QMidAera
    void onCloseControleAlunos();
    void onCloseTreinos();
    void onCloseFinanceiro();
    void onCloseFerramentas();
    void onCloseRelatorios();
    void onCloseSobre();
};

#endif // MAINWINDOW_H
