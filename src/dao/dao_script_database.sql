-- Tabelas sistema HealthUp System Adm
-- Tabela de alunos

/*
  STATUS = 0x00000001 -- Aluno ativo  
  STATUS = 0x00000002 -- Aluno inativo
*/
CREATE TABLE t_alunos (u_id INTEGER NOT NULL PRIMARY KEY ASC
                     , s_nome TEXT NOT NULL
                     , s_endereco TEXT NOT NULL
                     , s_bairro TEXT NOT NULL
                     , s_cidade TEXT NOT NULL
                     , u_cep INTEGER
                     , s_uf TEXT NOT NULL
                     , u_fone INTEGER NOT NULL
                     , u_celular INTEGER NOT NULL
                     , s_sexo TEXT NOT NULL
                     , u_cpf INTEGER
                     , u_rg INTEGER
                     , s_orgao_exp TEXT
                     , s_mail TEXT
                     , u_nascimento INTEGER NOT NULL
                     , u_idade INTEGER NOT NULL
                     , u_status INTEGER);                     


CREATE TABLE t_frequencia_alunos (u_id INTEGER NOT NULL PRIMARY KEY ASC
                                , u_id_aluno INTEGER NOT NULL
                                , u_entrada INTEGER NOT NULL
                                , u_saida INTEGER NOT NULL
                                , u_status INTEGER);                                

INSERT OR REPLACE INTO t_alunos VALUES (1, "Junior", "Rua Livreiro Arlindo, 135", "Farias Brito", "Fortaleza", 60011320, "Ce", 32140976, 96719272, "Masculino", 0, 0, "", "junior.jvm@gmail.com", 12061985, 30, 0);

DELETE FROM t_alunos WHERE u_id = 2; -- DELETE por ID or Matricula

DELETE FROM t_alunos;

SELECT * FROM t_alunos;
SELECT u_id FROM t_alunos ORDER BY u_id DESC LIMIT 1; -- ID �ltima Matricual


 INSERT INTO t_alunos VALUES ( ?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17 );
 
 INSERT INTO t_alunos VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? );   

 INSERT INTO t_alunos VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ); 


SELECT u_id
     , s_nome
     , s_endereco || ', ' || s_bairro AS s_endereco
     , u_fone || '/ ' || u_celular AS u_telefone      
     , u_status
  FROM t_alunos;


