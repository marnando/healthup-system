#include "connection.h"
#include "src/utils/defines.h"
#include <QMessageBox>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QDebug>
#include <QApplication>

Connection * Connection::m_connection = nullptr;
Connection::Connection(QObject *parent) : QObject(parent)
{

}

Connection::~Connection()
{
    Connection::releaseDB();
}

void Connection::createConnection()
{
    if (!m_connection)
    {
        m_connection = new Connection();
    }
}

bool Connection::createConnectionDb()
{
    // Conecto o database
    m_db = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"));

    // Seto um nome para o database
    m_db->setDatabaseName("healthup_database.db3"); //Mundar o nome do DATABASE é necessário

    // Verifico se consigo carregá-lo
    if (!m_db->open()) {

        QMessageBox::critical(0
                            , tr("Não pode abrir um Database")
                            , tr("Não há uma conexão estável para um database.\n"
                                 "Necessário suporte ao banco de dados. "
                                 "Por favor, entre em contato com o desenvolvedor.\n\n"
                                 "Clique em cancelar para finalizar.")
                            , QMessageBox::Cancel);

        return false;
    }

    qDebug() << "Database ok!";
    return true;
}

Connection * Connection::instance()
{
    createConnection();
    return m_connection;
}

QSqlDatabase * Connection::instanceDB()
{
    createConnection();

    if (!m_db)
    {
        createConnectionDb();
    }

    return m_db;
}

void Connection::releaseDB()
{
    if (m_db)
    {
        m_db->close();
        delete m_db;
        m_db = nullptr;
    }

    if (m_connection)
    {
        delete m_connection;
        m_connection = nullptr;
    }
}

int Connection::getNextId()
{
    int id = 0;
    QString sql = QString(" SELECT u_id FROM t_alunos ORDER BY u_id DESC LIMIT 1; ");
    QSqlQuery query = QSqlQuery(sql, *m_db);
    if (query.exec(sql))
    {
        query.next();
        id = query.value("u_id").toInt();
    } else
    {
        qDebug () << "Error: Connection::getNextId() -> " << query.lastError();
    }

    return ++id;
}

bool Connection::insert(QString a_table, QList<QVariant> a_listOfData, QList<QString>  a_listOfBind)
{
    //insert or replace into ? values (?, ?, ?, ...);
    bool saved = false;
    QString fields;

    //Verifico a consistência dos dados e dos binds
    if (a_listOfBind.size() != a_listOfData.size())
    {
        QMessageBox::information(nullptr,
                                 tr("Atenção"),
                                 tr("Erro no salvamento dos dados, dados incompletos. Entre em contato com o desenvolvedor do sistema."),
                                 QMessageBox::Ok);
        return saved;
    }

    //Inserindo dados no banco
    if (m_db->transaction())
    {
        int nextId = getNextId();
        QString sql = QString(" INSERT OR REPLACE INTO :table VALUES ( :fields ); ");

        //Populando os valores na tabela
        fields = "?, "; //id

        for (int cont = 0; cont < a_listOfData.size(); ++cont) {
            if (cont == (a_listOfData.size() - 1))
            {
                fields += "?";
            } else
            {
                fields += "?, ";
            }
        }

        QSqlQuery query = QSqlQuery(*m_db);
        sql.replace(":table", a_table);
        sql.replace(":fields", fields);
        query.prepare(sql);

        int pos = 0;
        int contador = 0;

        query.bindValue(contador, nextId);
        contador++;

        for (contador; contador <= a_listOfData.size(); ++contador)
        {
            if (a_listOfBind[pos].contains("s_"))
            {
                query.bindValue(contador, a_listOfData[pos].toString());
            } else
            if (a_listOfBind[pos].contains("u_"))
            {
                query.bindValue(contador, a_listOfData[pos].toInt());
            } else
            if (a_listOfBind[pos].contains("d_"))
            {
                query.bindValue(contador, a_listOfData[pos].toDouble());
            }

            pos++;
        }

        saved = query.exec();
        if (saved)
        {
            m_db->commit();
            QMessageBox::information(nullptr, tr("Atenção"), tr("Registro salvo com sucesso!"), QMessageBox::Ok);
        } else
        {
            qDebug() << "Error m_db: Connection::insert -> " << m_db->lastError();
            qDebug() << "Error query: Connection::insert -> " << query.lastError();
            qDebug() << "Error query sql: Connection::insert -> " << query.lastQuery();
            m_db->rollback();
            QMessageBox::information(nullptr, tr("Atenção"), tr("Erro no salvamento dos dados, por favor, repita a operação!"), QMessageBox::Ok);
        }
    }

    return saved;
}

QList<QList<QVariant> > Connection::selectAll(QString a_table, QList<QVariant> a_listOfData, QList<QString> a_listOfBind)
{
    bool showed = false;
    QList<QList<QVariant> > listRecord = QList<QList<QVariant> >();

    //Verificando a consistência dos dados e dos binds
//    if (a_listOfBind.size() != a_listOfData.size())
//    {
//        QMessageBox::information(nullptr,
//                                 tr("Atenção"),
//                                 tr("Erro no salvamento dos dados, dados incompletos. Entre em contato com o desenvolvedor do sistema."),
//                                 QMessageBox::Ok);
//        return listRecord;
//    }

    //Carregando dados no banco
    if (m_db->transaction())
    {

        QString sql = QString(" SELECT u_id "
                              "      , s_nome "
                              "      , s_endereco || ' - ' || s_bairro AS s_endereco "
                              "      , u_fone || '/ ' || u_celular AS u_telefone "
                              "      , u_status "
                              "   FROM :table "
                              "  ORDER BY u_status :order; ");

        QSqlQuery query = QSqlQuery(*m_db);
        sql.replace(":table", a_table);
        sql.replace(":order", QString("ASC"));
//        query.prepare(sql);
        showed = query.exec(sql);

        if (showed)
        {
            //Carrego os dados do model
            while(query.next())
            {
                listRecord.append(QList<QVariant>() << query.value("u_id")
                                                    << query.value("s_nome")
                                                    << query.value("s_endereco")
                                                    << query.value("u_telefone"));
            }


            m_db->commit();
        } else
        {
            m_db->rollback();
        }
    }

    return listRecord;
}
























