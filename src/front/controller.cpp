#include "controller.h"
#include "src/control/ccontrolealunos.h"
#include "src/view/mainwindow.h"

Controller::Controller(QObject *parent)
    : QObject(parent),
      m_mainWindow(nullptr),
      m_controlAlunos(nullptr)
{
}

Controller::~Controller()
{
    if (m_mainWindow) { delete m_mainWindow; m_mainWindow = nullptr; }
    if (m_controlAlunos) { delete m_controlAlunos; m_controlAlunos = nullptr; }
}

void Controller::initApp()
{
    if (createConnection())
    {
        initializeMainWindow();
    } else
    {
        exit(0);
    }
}

bool Controller::createConnection()
{
    return Connection::instance()->createConnectionDb();
}

void Controller::initializeMainWindow()
{
    m_mainWindow = new MainWindow();
    m_mainWindow->showMaximized();
    connect( m_mainWindow, SIGNAL(createAlunoControler()), SLOT(onCreateAlunoControler()) );
}

void Controller::onCreateAlunoControler()
{
    if (m_controlAlunos)
    {
        delete m_controlAlunos;
        m_controlAlunos = nullptr;
    }

    m_controlAlunos = new CControleAlunos();
    connect( m_mainWindow->controleAlunos(), SIGNAL(salvarCadastroAluno(QMap<quint32,QString>)), m_controlAlunos, SLOT(onSalvarCadastro(QMap<quint32,QString>)) );
    connect( m_mainWindow->controleAlunos(), SIGNAL(criarListaAlunos()), m_controlAlunos, SLOT(onCriarListaControleAlunos()) );
    connect( m_mainWindow->controleAlunos(), SIGNAL(criarListaAlunosDebito()), m_controlAlunos, SLOT(onCriarListaAlunosDebito()) );
    connect( m_mainWindow->controleAlunos(), SIGNAL(criarListaAlunosAusentes()), m_controlAlunos, SLOT(onCriarListaAlunosAusentes()) );
    connect( m_controlAlunos, SIGNAL(listDataControleAlunos(QList<QList<QVariant> >)), m_mainWindow->controleAlunos(), SLOT(onCriarListaControleAlunos(QList<QList<QVariant> >)) );
    connect( m_controlAlunos, SIGNAL(listDataAlunosDebito(QList<QList<QVariant> >)), m_mainWindow->controleAlunos(), SLOT(onCriarListaAlunosDebito(QList<QList<QVariant> >)) );
    connect( m_controlAlunos, SIGNAL(listDataAlunosAusentes(QList<QList<QVariant> >)), m_mainWindow->controleAlunos(), SLOT(onCriarListaAlunosAusentes(QList<QList<QVariant> >)) );
}

