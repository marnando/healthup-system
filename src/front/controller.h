#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include "src/dao/connection.h"
#include "src/utils/defines.h"

class CControleAlunos;
class MainWindow;
class Controller : public QObject
{
    Q_OBJECT

private:
    MainWindow * m_mainWindow;
    CControleAlunos * m_controlAlunos;

public:
    explicit Controller(QObject *parent = 0);
    ~Controller();

    void initApp();

private:
    bool createConnection();
    void initializeMainWindow();

signals:

public slots:
    void onCreateAlunoControler();
};

#endif // CONTROLLER_H
