#include "src/front/controller.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Controller controller;
    controller.initApp();

    return a.exec();
}
